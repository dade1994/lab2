package edu.ucsd.cs110w.temperature;
public class Celsius extends Temperature
{
  public Celsius(float t)
  {
    super(t);
  } 
  public String toString()
  {
	  return this.getValue() + " C";
  }
@Override
public Temperature toCelsius() {
	return this;
}
@Override
public Temperature toFahrenheit() {
	return new Fahrenheit(this.getValue()*9/5 + 32);
}
@Override
public Temperature toKelvin() {
	return new Kelvin((float)(this.getValue() + 273.15));
}
}