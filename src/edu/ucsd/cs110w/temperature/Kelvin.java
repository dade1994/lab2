/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * TODO (ded004): write class javadoc
 *
 * @author ded004
 */
public class Kelvin extends Temperature {
	public Kelvin(float t)
	{
		super(t);
	}
	public String toString()
	{
		
		return this.getValue() + " K";
	}
	@Override
	public Temperature toCelsius() {
		return new Celsius((float) (this.getValue()-273.15));
	}
	@Override
	public Temperature toFahrenheit() {
		
		return new Fahrenheit( (float)(1.8 *(this.getValue() - 273) + 32));
	}
	@Override
	public Temperature toKelvin() {
		return this;
	}
}
