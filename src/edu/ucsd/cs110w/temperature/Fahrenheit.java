package edu.ucsd.cs110w.temperature;
public class Fahrenheit extends Temperature
{
  public Fahrenheit(float t)
  {
    super(t);
  }
  public String toString()
  {
  return this.getValue()+" F";
  }
@Override
public Temperature toCelsius() {
	return new Celsius((this.getValue()-32) * 5/9);
}
@Override
public Temperature toFahrenheit() {
	return this;
}
@Override
public Temperature toKelvin() {
	return new Kelvin((this.getValue()-32) * 5/9 - 273);
}
}